﻿using UnityEngine;
using System.Collections;

public class ShowScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int score = PlayerPrefs.GetInt("score");
		GUIText text = GetComponent<GUIText>();
		text.text += " " + score;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
