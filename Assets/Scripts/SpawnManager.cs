﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {
	
	void OnEnable() {
		PokeSpawner.OnSpawned += CountSpawn ;
	}
	
	void OnDisable() {
		PokeSpawner.OnSpawned -= CountSpawn ;
	}
	
	public float spawnTime = 0.1f;
	public int spawnsLimit = 10 ;
	public int spawns ;
	
	public int level = 0 ;
	public int killedSpawnCount ;
	public float[] arraySpawnsTimeThresholdPropagation = new float[4];
	public int[] arraySpawnsCountThresholdPropagation = new int[4];
	
	public int activeSpawnLimit = 10 ;
	
	public delegate void Action ();
	public delegate void LevelAction (int level) ;
	
	
	public static event LevelAction OnLevelChanged ;
	public static event Action OnActiveSpawnLimitReached ;
	public static event Action OnFinalLevelReached ;
	public static event LevelAction OnSpawnCountChanged ;
	
	// Use this for initialization
	void Start () {
		spawns = 1 ;
		spawnTime = arraySpawnsTimeThresholdPropagation[0];
		PokeSpawner spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent("PokeSpawner") as PokeSpawner ;
		spawner.SetSpawnInterval(arraySpawnsTimeThresholdPropagation[level]);
	}
	
	public int activeSpawnsCount  {
		get { return spawns -	killedSpawnCount;}
	}
	

	
	void levelUp() {
		PokeSpawner spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent("PokeSpawner") as PokeSpawner ;
		if(level == arraySpawnsCountThresholdPropagation.Length -1 )
		{
			spawner.stop();
			if(OnFinalLevelReached != null)
				OnFinalLevelReached();
			
		}
		else 
		level++;	
		if(OnLevelChanged != null)
			OnLevelChanged(level);
		spawner.linearSpawnActive = true ;
		spawnTime = arraySpawnsTimeThresholdPropagation[level];
		spawner.SetSpawnInterval(spawnTime);
	}
	
	void CountSpawn() {
		
		spawns++;
		if(activeSpawnsCount >= activeSpawnLimit) {
			if(OnActiveSpawnLimitReached != null )
				OnActiveSpawnLimitReached();
			PokeSpawner spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent("PokeSpawner") as PokeSpawner ;
			spawner.stop();
			return ;
		}
	
		if(spawns >= arraySpawnsCountThresholdPropagation[level]) {
			levelUp();
		}
	}
	
	public void SpawnKilled() {
		gameObject.audio.Play();
		killedSpawnCount++;
		if(OnSpawnCountChanged != null) {
			OnSpawnCountChanged(killedSpawnCount);
		}
	}
	
}
