﻿using UnityEngine;
using System.Collections;

public class GameEnd : MonoBehaviour {
	
	
	public static event System.Action OnGameEnd ;
	public static event System.Action OnRestartingGame ;	
	
	void OnEnable () {
		SpawnManager.OnActiveSpawnLimitReached += debriefing ;
	}
	
	void OnDisable () {
		SpawnManager.OnActiveSpawnLimitReached -= debriefing;
	}
	
	void debriefing() {
		
		
		SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent("SpawnManager") as SpawnManager ;
		int spawnsKilled = spawnManager.killedSpawnCount;
		SpawnManager.OnActiveSpawnLimitReached -= debriefing;

		int best = PlayerPrefs.GetInt("best");
		if (best < spawnsKilled)
			PlayerPrefs.SetInt("best",spawnsKilled);

		PlayerPrefs.SetInt("score",spawnsKilled);
		PlayerPrefs.Save();

		if(OnRestartingGame != null)
			OnRestartingGame();
		if(OnGameEnd  != null )
			OnGameEnd();
		Invoke("EndGameScript",2.0f);
	}
	
	void EndGameScript() {
	
		Application.LoadLevel("score");
	}
}
