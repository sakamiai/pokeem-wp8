﻿using UnityEngine;
using System.Collections;

public class StartWhenPoked : MonoBehaviour {
	
	
	public delegate void Action () ;
	public static event Action OnStartPokePoked ;
	
	
	
	void OnMouseDown () {
		
		if(OnStartPokePoked != null)
			OnStartPokePoked();
		
	}
	
	void OnMouseOver () {
		
		if(OnStartPokePoked != null)
			OnStartPokePoked();
		
	}
}
