﻿using UnityEngine;
using System.Collections;

public class OnEscPressed : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("escape"))
		{
			string currentScene = Application.loadedLevelName;
			switch(currentScene) {

			case "language":
				Debug.LogWarning("exiting app");
				Application.Quit();
				break;
			case "score":
				GoBackToLanguageScreen();
				break;
			case "game":
				GoBackToLanguageScreen();
				break ;
			
			}
					}
	}

	void GoBackToLanguageScreen() {
		Application.LoadLevel("language");
	}
}
