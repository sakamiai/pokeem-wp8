﻿using UnityEngine;
using System.Collections;

public class PokeSpawner : MonoBehaviour
{
	
	
	
	private int spawnCounter ;
	private float timeCounter = 0f ;
	public GameObject pokeObject ;
	public float spawnInterval = 1f;
	public float minXcoordinate = -10f;
	public float maxXcoordinate = 10f;
	public float minZcoordinate = -5f;
	public float maxZcoordinate = 5f;
	public GameObject xMark ;
	public GameObject zMark ;
	public bool isActive = false ;
	private int linearSpawnCount = 10 ;
	public bool linearSpawnActive = true;
	
	
	public float[] linearSpawnXCoordinates = new float [10];
	
	
	public delegate void SpawnAction ();
	public static event SpawnAction OnSpawned ;
	public delegate void Action ();
	public static event Action OnLinearSpawnStopped ;
	
	
	// Use this for initialization
	void Start ()
	{
		if (pokeObject == null) {
			Debug.LogWarning ("PokeSpawner.cs: no pokeObject set, can't initialize spawning.");
			return;
		}
		
		// sets the x and y coordinate limits if the x and yMark are available
		// the plane has to be at 0,0 coordinate
		if (xMark != null && zMark != null) {
			minXcoordinate = xMark.transform.position.x;
			maxXcoordinate = - xMark.transform.position.x;
			minZcoordinate = zMark.transform.position.z;
			maxZcoordinate = - zMark.transform.position.z;
		} else {
			Debug.LogWarning (this.name + ":x and y Mark not set. Falling back to default coordinates");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (! isActive)
			return ;
	
		timeCounter += Time.deltaTime;
		
		if (timeCounter >= spawnInterval) {
			timeCounter -= spawnInterval;
			Spawn ();
		}
	}
	
	public void Spawn ()
	{
		if (linearSpawnActive) {
			// Alkaa vasemmasta oikealle
			Instantiate (pokeObject, new Vector3 (linearSpawnXCoordinates[10-linearSpawnCount], pokeObject.transform.position.y, -2), pokeObject.transform.rotation);
			if (OnSpawned != null)
				OnSpawned ();
			if(--linearSpawnCount <= 0 ) {
				linearSpawnCount = 10 ;
				linearSpawnActive = false ;
				if(OnLinearSpawnStopped != null)
					OnLinearSpawnStopped();
			}
		} else {
			float coordinateX = UnityEngine.Random.Range (minXcoordinate, maxXcoordinate);
			float coordinateZ = UnityEngine.Random.Range (minZcoordinate, maxZcoordinate);
			Instantiate (pokeObject, new Vector3 (coordinateX, pokeObject.transform.position.y, coordinateZ), pokeObject.transform.rotation);
			if (OnSpawned != null)
				OnSpawned ();
		}
		spawnCounter++;
		
	}
	
	public void activate ()
	{
		isActive = true;
	}

	public void stop ()
	{
		isActive = false;
		timeCounter = 0;
	}
	
	public void DecreaseSpawnIntervalBy (float seconds)
	{
		spawnInterval -= seconds;
	}
	public void SetSpawnInterval (float seconds)
	{
		spawnInterval = seconds;
	}
}
