﻿using UnityEngine;
using System.Collections;

public class TerrainAnimationBackward : MonoBehaviour {

		void OnEnable() {
		RestartGamePressed.OnRestartingGame += AnimateBackward;
	}
	
	void OnDisable() {
		RestartGamePressed.OnRestartingGame	-= AnimateBackward;
	}
	
	void AnimateBackward () {
		gameObject.animation.Play("colorAnimationBack");

	}
}
