﻿using UnityEngine;
using System.Collections;

public class PokeCounter : MonoBehaviour {
	
	public int spawnsCount = 0 ;
	
	
	void OnEnable() {
		SpawnManager.OnSpawnCountChanged += ChangeCount ;
	}
	
	void OnDisable() {
		SpawnManager.OnSpawnCountChanged -= ChangeCount ;
	}
	
	private void ChangeCount(int spawns) {
		spawnsCount = spawns;
		this.GetComponent<GUIText>().text = spawnsCount + "" ;
	}
}
