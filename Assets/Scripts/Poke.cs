﻿using UnityEngine;
using System.Collections;


public class Poke : MonoBehaviour {

	public int scaleLimit = 10 ;

	
	void OnEnable () {
		SpawnManager.OnActiveSpawnLimitReached += debriefing ;
	}
	
	void OnDisable () {
		
		SpawnManager.OnActiveSpawnLimitReached -= debriefing;
	}
	
	void debriefing() {
		RemovePokedScript();
		// endlessExpansionOn = true ;
		
	}
	
	void RemovePokedScript() {
		 GetComponent<Poked>().enabled = false ;
	}
	
	void Update () {
//		if(endlessExpansionOn)
//		{
//			if(animation.IsPlaying("animation"))
//				animation.Stop();
//			
//			if (this.transform.localScale.x > scaleLimit)
//				return ;
//			transform.localScale += new Vector3(transform.localScale.x *Time.deltaTime,transform.localScale.y,transform.localScale.z* Time.deltaTime);
//		}
	}
}
