﻿using UnityEngine;
using System.Collections;

public class RestartGamePressed : MonoBehaviour {
	
	public delegate void Action () ;
	public static event Action OnRestartingGame;
	
	void OnMouseDown () {
		
		if(OnRestartingGame != null)
			OnRestartingGame();
		Debug.LogWarning("Restarting");
		Invoke ("restartGame",2.0f);
		if(gameObject.audio.enabled)
			gameObject.audio.Play();
	}
	
	void restartGame() {
		
		Application.LoadLevel("game");
	}
}
