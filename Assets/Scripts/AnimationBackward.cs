﻿using UnityEngine;
using System.Collections;

public class AnimationBackward : MonoBehaviour {

	void OnEnable() {
		RestartGamePressed.OnRestartingGame += AnimateBackward;
	}
	
	void OnDisable() {
		RestartGamePressed.OnRestartingGame	-= AnimateBackward;
	}
	
	void AnimateBackward () {
		
		gameObject.animation.Play("animation1Back");
	}
}
