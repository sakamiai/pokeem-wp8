﻿using UnityEngine;
using System.Collections;

public class GuiTextScript : MonoBehaviour {

	public float fontSize = 10.0f;
	private float screenHight = 1080.0f ;
	public float virtualScreenHeight = 1080.0f;
	
	// Use this for initialization
	void Start () {
		screenHight = Screen.height;
		RedrawText();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		if (Screen.height != screenHight) {
			screenHight = Screen.height ;
			RedrawText();
		}
	}
	
	void RedrawText() {
		GUIText text = this.GetComponent("GUIText") as GUIText;
			float realFontSize = (screenHight / virtualScreenHeight) * fontSize ;
			text.fontSize =  (int)realFontSize;
	}
}
