﻿using UnityEngine;
using System.Collections;

public class TerrainClick : MonoBehaviour {

	void OnMouseDown() {
		
		PokeSpawner spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent("PokeSpawner") as PokeSpawner ;
		if(!spawner.isActive)
			return ;
		spawner.Spawn();	
	}
}
