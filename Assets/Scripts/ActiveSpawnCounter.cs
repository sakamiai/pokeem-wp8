﻿using UnityEngine;
using System.Collections;

public class ActiveSpawnCounter : MonoBehaviour {
	
	public SpawnManager sm ;
	public string character ;
	
	// Use this for initialization
	void Start () {
		 
	}
	
	// Update is called once per frame
	void Update () {
		switch(sm.activeSpawnsCount) {
		case 0 :
			GetComponent<GUIText>().text ="";
			GetComponent<GUIText>().color = Color.green;
			break;	
		case 1 :
			GetComponent<GUIText>().text =character;
			GetComponent<GUIText>().color = Color.green;
			break;
		case 2 :
			GetComponent<GUIText>().text =character+character;
			GetComponent<GUIText>().color = Color.green;
			break;
		case 3 :
			GetComponent<GUIText>().text =character+character+character;
			GetComponent<GUIText>().color = Color.green;
			break;
		case 4 :
			GetComponent<GUIText>().text =character+character+character+character;
			GetComponent<GUIText>().color = Color.green;
			break;
		case 5 :
			GetComponent<GUIText>().text =character+character+character+character+character;
			GetComponent<GUIText>().color = Color.yellow;
			break;
		case 6 :
			GetComponent<GUIText>().text =character+character+character+character+character+character;
			GetComponent<GUIText>().color = Color.yellow;
			break;
		case 7 :
			GetComponent<GUIText>().text =character+character+character+character+character+character+character;
			GetComponent<GUIText>().color = Color.yellow;
			break;
		case 8 :
			GetComponent<GUIText>().text =character+character+character+character+character+character+character+character;
			GetComponent<GUIText>().color = Color.red;
			break;
		case 9 :
			GetComponent<GUIText>().text =character+character+character+character+character+character+character+character+character;
			GetComponent<GUIText>().color = Color.red;
			break;
		case 10 :
			GetComponent<GUIText>().text =character+character+character+character+character+character+character+character+character+character;
			GetComponent<GUIText>().color = Color.red;
			break;
		default :
				break;
				
		}

	}
}
