﻿using UnityEngine;
using System.Collections;

public class SelectLanguageScript : MonoBehaviour {
	
	public string LanguageID ;
	
	public delegate void LanguageSelectedAction (string language);
	public static event LanguageSelectedAction OnLanguageSelected;
	
	void OnMouseDown() {
		gameObject.audio.Play();
		if(OnLanguageSelected != null)
			OnLanguageSelected(LanguageID);
		
	}
}
