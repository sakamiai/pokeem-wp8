﻿using UnityEngine;
using System.Collections;

public class SaveLanguage : MonoBehaviour {

	void OnEnable() {
			SelectLanguageScript.OnLanguageSelected += SaveLanguageToMemory;
	}
	
	void OnDisable() {
		SelectLanguageScript.OnLanguageSelected -= SaveLanguageToMemory;
	}
	
	public void SaveLanguageToMemory(string language) {
			PlayerPrefs.SetString("language",language);
		Debug.LogWarning(language);
			PlayerPrefs.Save();
		
		
	}
}
