﻿using UnityEngine;
using System.Collections;

public class SetTextLanguage : MonoBehaviour {
	
	public string Key = "" ;
	
	// Use this for initialization
	void Awake () {
		
		if(Key == "" ) Key = this.name ;
		
		GUIText mesh = GetComponent<GUIText>();
		mesh.text = strings.GetString(Key);
	}
	
}
