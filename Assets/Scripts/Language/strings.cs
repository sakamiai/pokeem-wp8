﻿using UnityEngine;
using System.Collections;

public class strings : MonoBehaviour {

	public static string [,] tableEN = new string[,] {
	{"PokeText" 	  ,"Poke'em !"},
	{"PokeText1"	  ,"Poke me, and me, and me..."},
	{"PokeText2" 	  ,"10 is too many, poke !"},
	{"PokeText3" 	  ,"Don't poke everywhere !"},
	{"PokeText4" 	  ,"Speed increased !"},
	{"PokeText5" 	  ,"Still here ? Here is more ..."},
	{"PokeText6" 	  ,"This doesn't end, you know ?"},
	{"PokeText7"	  ,"Speed increased !" },
	{"PokeText8" 	  ,"Keep going !"},
	{"PokeText9" 	  ,"Almost there !"},
	{"PokeText10" 	  ,"0.2 sec interval per spawn !"},	
	{"Best" 		  ,"Best:"},
	{"Poked" 		  ,"Poked:"},
	{"Retry" 		  ,"Retry !"}
	};
	
	public static string [,] tableFI = new string[,] {
	{"PokeText" 	  ,"Poke'em!"},
	{"PokeText1"	  ,"Ja seuraava, ja seuraava, ja..."},
	{"PokeText2" 	  ,"10 on liikaa, jatka !"},
	{"PokeText3" 	  ,"Älä koske valkoista aluetta!"},
	{"PokeText4" 	  ,"Vauhtia lisätty!"},
	{"PokeText5" 	  ,"Vielä täällä ? Tässä lisää ..."},
	{"PokeText6" 	  ,"Tämä ei lopu ikinä !"},
	{"PokeText7"	  ,"Vauhtia lisätty!" },
	{"PokeText8" 	  ,"Jatka samaan tapaan !"},
	{"PokeText9" 	  ,"Melkein perillä !"},
	{"PokeText10" 	  ,"0.2 sekuntin väli!"},	
	{"Best" 		  ,"Paras:"},
	{"Poked" 		  ,"Saldo:"},
	{"Retry" 		  ,"Uusi!"}
	};
	
	public static string [,] tableFR = new string[,] {
	{"PokeText" 	  ,"Poke'em !"},
	{"PokeText1"	  ,"Moi aussi, moi aussi, moi aussi..."},
	{"PokeText2" 	  ,"S'il y en a 10 tu meurs, continue !"},
	{"PokeText3" 	  ,"Ne touche pas n'importe ou !"},
	{"PokeText4" 	  ,"La vitesse augmente !"},
	{"PokeText5" 	  ,"La vitesse augmente !"},
	{"PokeText6" 	  ,"La vitesse augmente !"},
	{"PokeText7"	  ,"La vitesse augmente !" },
	{"PokeText8" 	  ,"La vitesse augmente !"},
	{"PokeText9" 	  ,"La vitesse augmente !"},
	{"PokeText10" 	  ,"Un nouveau tous les 0.2 secondes !"},	
	{"Best" 		  ,"Meilleur:"},
	{"Poked" 		  ,"Total:"},
	{"Retry" 		  ,"Encore !"}
	};
	
	public static string GetString(string key) {
		string language = PlayerPrefs.GetString("language");
		string [,] table ;
		switch(language) {
		case "EN" :
			table = tableEN;
			break;
		case "FI" :
			table = tableFI;
			break;
		case "FR" :
			table = tableFR;
			break;
		default :
			table = tableEN ;
			break ;
		}
		
		for(int i=0 ; i < table.GetLength(0);i++) {
			if(table[i,0] == key) return table[i,1];
		}
		
		return "nan" ;
	}
}
