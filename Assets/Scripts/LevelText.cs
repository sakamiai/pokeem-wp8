﻿using UnityEngine;
using System.Collections;

public class LevelText : MonoBehaviour {

	void OnEnable()  {
		PokeSpawner.OnLinearSpawnStopped += HandleSpawnManagerOnLevelChanged;	
	}
	
	void OnDisable() {
		PokeSpawner.OnLinearSpawnStopped -= HandleSpawnManagerOnLevelChanged;
	}

	void HandleSpawnManagerOnLevelChanged ()
	{
		gameObject.SetActive(false);
	}
}
