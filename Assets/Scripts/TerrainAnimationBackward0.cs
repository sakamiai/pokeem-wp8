﻿using UnityEngine;
using System.Collections;

public class TerrainAnimationBackward0 : MonoBehaviour {

	void OnEnable() {
		GameEnd.OnGameEnd += AnimateBackward;
	}
	
	void OnDisable() {
		GameEnd.OnGameEnd	-= AnimateBackward;
	}
	
	void AnimateBackward () {
		gameObject.animation.Play("colorAnimationBack");
	}
}
