﻿using UnityEngine;
using System.Collections;

public class StartPokePoked : MonoBehaviour {

	
	void OnEnable () {
		Poked.OnPoked += destroyThis;
	}
	
	void OnDisable () {
		Poked.OnPoked -= destroyThis;
	}
	
	void destroyThis() {
		
		Destroy(gameObject);
	}
}
