﻿using UnityEngine;
using System.Collections;

public class Poked : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public delegate void PokedAction ();
	public static event PokedAction OnPoked ;
	
	void OnMouseOver() {
		if(!this.enabled) return ;
		SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent("SpawnManager") as SpawnManager ;
		spawnManager.SpawnKilled();
		if(OnPoked != null )
			OnPoked();
		Destroy(gameObject);
	}

}
