﻿using UnityEngine;
using System.Collections;

public class GameRunning : MonoBehaviour {
	
	public GameObject[] TextOne;
	public GameObject spawner ;
	
	void OnEnable()  {
		SpawnManager.OnLevelChanged += HandleSpawnManagerOnLevelChanged;	
		StartWhenPoked.OnStartPokePoked += RunGame;
	}
	
	void OnDisable() {
		SpawnManager.OnLevelChanged -= HandleSpawnManagerOnLevelChanged;
		StartWhenPoked.OnStartPokePoked -= RunGame;
	}
	
	void RunGame() {
		if(spawner == null)
			spawner = GameObject.FindGameObjectWithTag("PokeSpawner");
		PokeSpawner sm = spawner.GetComponent<PokeSpawner>(); 
		TextOne[0].SetActive(true);
		sm.linearSpawnActive = true ;
		sm.activate();	
	}

	void HandleSpawnManagerOnLevelChanged (int level)
	{
		switch (level) {
		case 0 :
			TextOne[0].SetActive(true);
			break;
		case 1 :
			TextOne[1].SetActive(true);
			break;
			case 2 :
			TextOne[2].SetActive(true);
			break;
			case 3 :
			TextOne[3].SetActive(true);
			break;
			case 4 :
			TextOne[4].SetActive(true);
			break;
			case 5 :
			TextOne[5].SetActive(true);
			break;
		case 6 :
			TextOne[6].SetActive(true);
			break;
		case 7 :
			TextOne[7].SetActive(true);
			break;
		case 8 :
			TextOne[8].SetActive(true);
			break;
		case 9 :
			TextOne[9].SetActive(true);
			break;
			
		default:
			TextOne[1].SetActive(true);
			break;
		}	
	}
	
	
	
}
